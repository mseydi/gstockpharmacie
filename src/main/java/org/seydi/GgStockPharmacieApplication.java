package org.seydi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GgStockPharmacieApplication {

	public static void main(String[] args) {
		SpringApplication.run(GgStockPharmacieApplication.class, args);
	}

}
